
<!DOCTYPE html>
<html>
<head>
	<title>Orquidea || Carrinho de Compras</title>
</head>
<body>

<?php
	session_start();


//	var_dump($_SESSION["nivel"]);
//	var_dump($_SESSION['user']);
//	var_dump($_SESSION['id']);


	  $SESSION["mer_id"] = $mer_id = $_GET['mer_id'];


//	$mer_id = $_GET['mer_id'];
	//		$SESSION['mer_id'] = $mer_id;
	//var_dump($mer_id);

	//require_once "functions/product.php";
	require("../../Controller/Carrinho/CarrinhoFunctions.php");

	// $pdoConnection = require_once "connection.php";

	if(isset($_GET['acao']) && in_array($_GET['acao'], array('add', 'del', 'up'))) {

		if($_GET['acao'] == 'add' && isset($_GET['id']) && preg_match("/^[0-9]+$/", $_GET['id'])){
			addCart($_GET['id'], 1);
		}

		if($_GET['acao'] == 'del' && isset($_GET['id']) && preg_match("/^[0-9]+$/", $_GET['id'])){
			deleteCart($_GET['id']);
		}

		if($_GET['acao'] == 'up'){
			if(isset($_POST['prod']) && is_array($_POST['prod'])){
				foreach($_POST['prod'] as $id => $qtd){
						updateCart($id, $qtd);
				}
			}
		}
		header('location: carrinho.php?mer_id='.$mer_id);
	}

	$resultsCarts = getContentCart();
	$totalCarts  = getTotalCart();

//var_dump($resultsCarts);
//var_dump($totalCarts);


?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" />

</head>
<body>
	<div class="container">
		<div class="card mt-5">
			 <div class="card-body">
	    		<h4 class="card-title">Carrinho</h4>
	    		<a href="compre.php?mer_id=<?php echo $mer_id ?>">Lista de Produtos</a>
	    	</div>
		</div>
	</div>


		<?php if($resultsCarts) : ?>
			<form action="carrinho.php?acao=up&mer_id=<?php echo $mer_id ?>"method="post">
</br>

			<table class="table table-strip">
				<thead>
					<tr>
						<th>Produto</th>
						<th>Quantidade</th>
						<th>Preço</th>
						<th>Subtotal</th>
						<th>Ação</th>

					</tr>
				</thead>
				<tbody>
				  <?php foreach($resultsCarts as $result) : ?>
					<tr>

						<td><?php echo $result['name']?></td>
						<td>
							<input type="text" name="prod[<?php echo $result['id']?>]" value="<?php echo $result['quantity']?>" size="1" />

							</td>
						<td>R$<?php echo number_format($result['price'], 2, ',', '.')?></td>
						<td>R$<?php echo number_format($result['subtotal'], 2, ',', '.')?></td>
						<td><a href="carrinho.php?acao=del&id=<?php echo $result['id']?>&mer_id=<?php echo $mer_id ?> " class="btn btn-danger">Remover</a></td>

					</tr>


				<?php endforeach;?>



				</tbody>

			</table>
			<center>
			<tr>
			 <td colspan="3" class="text-right"><b>Total: </b></td>
			 <td>R$<?php echo number_format($totalCarts, 2, ',', '.')?></td>
			</tr>
		</br>
		</br>



			<a class="btn btn-info" href="compre.php?mer_id=<?php echo $mer_id  ?>">Continuar Comprando</a>
			<button class="btn btn-primary" type="submit">Atualizar Carrinho</button>



		<!--	<a class="btn btn-info" href="./SetEntrega.php?mer_id="<?php // echo $mer_id."&" ?>>Finalizar Compra</a> -->

			<?php  ?>
		</br>
			</br>
			</form>
			<form method="post" action="../../Controller/Carrinho/Fechar_Compra.php?mer_id=<?php echo $mer_id; ?>" >

				Data de Entrega: <input id="date" name="date" type="date" required>
			</br>
				<input type="hidden" id="unam" name="unam" value="<?php echo $_SESSION['user'] ?>" >
				<input type="hidden" id="logid" name="logid" value="<?php echo $_SESSION['id'] ?>" >
					<input type="hidden" id="buyon" name="buyon" value="on" >
				</br>
				<button class="btn btn-primary" type="submit">Finalizar Compra</button>
			</form>


			</center>
	<?php endif?>

	</div>

	<?php $_SESSION['total'] = $totalCarts;  ?>
</body>

</html>
<script type="text/javascript">

	setTimeout(function Obs(){
		alert("Lembre-se: A Entrega Será Enviada Para o Endereço Vinculado a Essa Conta")
	}, 3000);


</script>

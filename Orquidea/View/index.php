    <head>
        <title>Orquidea || Login</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link href="../View/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
        <!-- Bootstrap CSS CDN -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
        <!-- Our Custom CSS -->
        <link rel="stylesheet" href="../View/assets/css/style2.css">
        <!-- Scrollbar Custom CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">

        <!-- Font Awesome JS -->
        <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
        <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>
    </head>
  <body>
              <header class = "header-1">

                    <div class = "logo">
                        <a href = "index.php"><img src="../View/Imagens/Logomarca.png" style="height: 95px; width: 95px; margin-left: -10px;"></a>
                    </div>
                <div class = "menu">    

        </header>  
  <div class="wrapper">
       <center>
         
            <main class = "col-100">           

           <div class="content">

            <br>
            <br>
            <br>
            <br>
            <div id="login">
                <div id="triangle"></div>
                <h1>Acesse</h1>
                <form method="POST" action="../Controller/login.php">
<center>
                      
                           <div class="col-sm-10" >
                               <input type="text" name="usuario" class="form-control" id="usuario" placeholder="Usuario"  
required>        </div>
            <br>

 
                           <div class="col-sm-10">
                               <input type="password" name="senha" class="form-control" id="senha" placeholder="Senha"  
required>        </div>

</center>  
            <br>
                   
                     <button type="submit" class="btn btn-primary">Entrar</button>
 
              </form>
Não possui uma conta? <a href="Cadastro.php">Cadastre-se</a>
            </div>
</br>







           </br>
            <div class="header-2">

            </div>
            </div>
         </main>
        </center>
    </div>
    <!--================End Login Box Area =================-->

    <!-- Bootstrap core JavaScript -->
    <script src="../View/vendor/jquery/jquery.min.js"></script>
    <script src="../View/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- jQuery CDN - Slim version (=without AJAX) -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <!-- jQuery Custom Scroller CDN -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#sidebar").mCustomScrollbar({
                theme: "minimal"
            });

            $('#sidebarCollapse').on('click', function () {
                $('#sidebar, #content').toggleClass('active');
                $('.collapse.in').toggleClass('in');
                $('a[aria-expanded=true]').attr('aria-expanded', 'false');
            });
        });
    </script>
</body>
</html>

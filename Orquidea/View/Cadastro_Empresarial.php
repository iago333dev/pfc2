<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Orquidea || Cadastro Empresarial</title>
        <!-- Bootstrap CSS CDN -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
        <!-- Our Custom CSS -->
        <link rel="stylesheet" href="../View/assets/css/style2.css">
        <!-- Scrollbar Custom CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
        <!-- Font Awesome JS -->
        <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
        <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>
        <!-- Bloqueia o Click Direito do Mouse-->
        <script LANGUAGE="JavaScript">
            function disableselect(e) {
                return false
            }
            function reEnable() {
                return true
            }
            document.onselectstart = new Function("return false")
            document.oncontextmenu = new Function("return false")

            if (window.sidebar) {
                document.onmousedown = disableselect
                document.onclick = reEnable
            }
        </script>
    </head>
    <body>
        <header class = "header-1">

                <div class = "logo">
                    <a><img src="../View/Imagens/Logomarca.png" style="height: 95px; width: 95px; margin-left: -10px;"></a>
                </div>
                <div class = "menu">    

        </header>        
        <div class="wrapper">
    <center>
            <main class = "col-100">        

            <br>
            <br>
            <br>
            <br>
            <div id="login">
                <div id="triangle"></div>
                <h1>Cadastre-se</h1>
                    <form method="POST" action="../Controller/Gerente/Core.php" >
                    Nome Da Empresa:</br><input type="text" name="wemnome" id="wemnome" required /></br>
                    CNPJ:</br><input type="text" id="cnpj" name="wcnpj" placeholder="Ex: 00.000.000/0000-**" required /></br>
                    Nome De Usuário:</br><input type="text" name="wnome" id="wnome" required /></br>
                    CPF:</br><input type="text" id="cpf" name="wcpf" placeholder="Ex: 000.000.000-00" required /></br>
                    Email:</br><input type="email" name="wemail" id="wemail" placeholder="Ex: email@email.com.br" required /></br>
                    Contato da Empresa 1: </br>
                    <input type="text" id="tel1" name="wtel1" placeholder="Ex: (DDD) 0000-0000" required /></br>
                    Contato da Empresa 2: </br>
                    <input type="text" id="tel2" name="wtel2" placeholder="Ex: (DDD) 90000-0000" required /></br>
                    CEP: </br>
                    <input type="text" id="cep" name="wcep" placeholder="Ex: 00.000-000" required /></br>           
                    Endereço: </br><input type="text" name="wender" id="wender" required></br>
                    Bairro: </br><input type="text" name="wbair" id="wbair" required></br>
                    Cidade: </br><input type="text" name="wcid" id="wcid" required></br>
                    UF: </br>
                    <select name="wuf" required>
                        <option value="AC">AC</option>
                        <option value="AL">AL</option>
                        <option value="AM">AM</option>
                        <option value="AP">AP</option>
                        <option value="BA" selected="">BA</option>
                        <option value="CE">CE</option>
                        <option value="AP">AP</option>
                        <option value="DF">DF</option>
                        <option value="AP">ES</option>
                        <option value="GO">GO</option>
                        <option value="MA">MA</option>
                        <option value="MT">MT</option>
                        <option value="MS">MS</option>
                        <option value="MG">MG</option>
                        <option value="AP">AP</option>
                        <option value="PA">PA</option>
                        <option value="PB">PB</option>
                        <option value="PR">PR</option>
                        <option value="PE">PI</option>
                        <option value="RJ">RJ</option>
                        <option value="RN">RN</option>
                        <option value="RS">RS</option>
                        <option value="RO">RO</option>
                        <option value="RR">RR</option>
                        <option value="SC">SC</option>
                        <option value="SP">SP</option>
                        <option value="SE">SE</option>
                        <option value="TO">TO</option>
                    </select></br>
                    Nacionalidade: </br><input type="text" name="wpais" id="wpais" required></br>
                    Data de Nascimento: </br><input type="date" name="wnasc" id="wnasc" required></br>
                    Novo Login: </br><input type="text" name="wlog" id="wlog" required></br>
                    Nova Senha: </br><input type=type="password" name="wsen" required></br>
                    <?php
                    $func = "Cadastro_Empresarial";
                    $func = md5($func);
                    ?>
                    <input type="hidden" name="cad" value=<?php echo"$func" ?>>
                    <input type="submit" value="Cadastrar">
                </form>
            </div>

        <div class="header-2">
        </div>
        </main>               
        <button onclick="Test()">AutoInput [Versão de Testes]</button>
    </center>
</div>
<!--================End Login Box Area =================-->
<!-- Bootstrap core JavaScript -->
<script src="../View/vendor/jquery/jquery.min.js"></script>
<script src="../View/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- jQuery CDN - Slim version (=without AJAX) -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<!-- Popper.JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
<!-- Bootstrap JS -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
<!-- jQuery Custom Scroller CDN -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript">
            $(document).ready(function () {
                $("#sidebar").mCustomScrollbar({
                    theme: "minimal"
                });

                $('#sidebarCollapse').on('click', function () {
                    $('#sidebar, #content').toggleClass('active');
                    $('.collapse.in').toggleClass('in');
                    $('a[aria-expanded=true]').attr('aria-expanded', 'false');
                });
            });
</script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.0/jquery.mask.js"></script>

<script>
    $(document).ready(function () { 
        var $wcnpj = $("#cnpj");
        $wcnpj.mask('00.000.000/0000-00', {reverse: true});
    });    
    $(document).ready(function () { 
        var $wcpf = $("#cpf");
        $wcpf.mask('000.000.000-00', {reverse: true});
    });
        $(document).ready(function () { 
        var $wtel1 = $("#tel1");
        $wtel1.mask('(00)0000-0000', {reverse: true});
    });
        $(document).ready(function () { 
        var $wtel2 = $("#tel2");
        $wtel2.mask('(00)00000-0000', {reverse: true});
    });
        $(document).ready(function () { 
        var $wcep = $("#cep");
        $wcep.mask('00.000-000', {reverse: true});
    });


        function Test(){
            document.getElementById('cnpj').value = "23.456.789/0123-45";
            document.getElementById('wemnome').value = "OrquideaEmpresa";
            document.getElementById('wnome').value = "OrquideaEmpresa";
            document.getElementById('cpf').value = "123.456.789-12";
            document.getElementById('wemail').value = "Cliente@Orquidea.com.br";
            document.getElementById('tel1').value = "(71)1234-5678";
            document.getElementById('tel2').value = "(71)5678-1234";
            document.getElementById('cep').value = "12-345-678";
            document.getElementById('wender').value = "Rua Empresa Orquidea";
            document.getElementById('wbair').value = "Bairro Empresa Orquidea";
            document.getElementById('wcid').value = "Cidade Empresa Orquidea";
            document.getElementById('wpais').value = "Brasil";
            document.getElementById('wnasc').value = "10081998";
            document.getElementById('wlog').value = "OrquideaEmpresa";
        }




</script>
</body>
</html>

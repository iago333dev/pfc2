<?php
//include '../../Controller/verificar.php';
//if ($_SESSION['nivel'] == 'Cliente') {
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Orquídea || Início</title>
        <!-- Bootstrap CSS CDN -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
        <!-- Our Custom CSS -->
        <link rel="stylesheet" href="../../View/assets/css/style2.css">
        <!-- Scrollbar Custom CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
        <!-- Font Awesome JS -->
        <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
        <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>
    </head>
    <body>

        <nav class="navbar navbar-expand-lg navbar-light" id="menu">
            <div class="navbar-brand">
                <a><img src="../../View/Imagens/Logomarca.png" style="height: 115px; width: 115px; margin-left: 45px;"></a>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#conteudoNavbarSuportado" aria-controls="conteudoNavbarSuportado" aria-expanded="false" aria-label="Alterna navegação">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="conteudoNavbarSuportado">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active" id="menu-item-active">
                        <a class="nav-link" href="#">Início <span class="sr-only">(página atual)</span></a>
                    </li>

                    <li class="nav-item dropdown" id="menu-item-active">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Menu
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">

                                <a class="dropdown-item" href="../View/Cliente/Perfil_Cliente.php">Perfil</a>
                            </br>
                            <?php if ($_SESSION['existe_estoque'] == 0) { ?>
                                <a class="dropdown-item" href="../../Controller/Gerente/Core.php">Estoque</a>
                            <?php } ?>
                            </br>
                            <?php if ($_SESSION['existe_estoque'] == 1) { ?>
                                <?php $_SESSION['criar_produto'] = 1 ?>
                                <a class="dropdown-item" href="../../Controller/Gerente/Core.php">Estoque</a>
                            <?php } ?>
                            </br>
                            <a class="dropdown-item" href="paginas/Vendas/lista.html">Vendas</a>
                        </div>
                    </li>
                    <li class="nav-item" id="menu-item-active">
                        <a class="nav-link" href="../../Controller/logout.php">Sair</a>
                    </li>
                    <li class="nav-item" id="menu-item-active">
                        <a class="nav-link" href="../Cliente/Perfil_Cliente.php">Perfil</a>
                    </li>
                    <li class="nav-item" id="menu-item-active">
                        <a class="nav-link" href="./MercadosDisponiveis.php">Comprar</a>
                    </li>
                </ul>
                <form class="form-inline my-2 my-lg-0" id="menu">
                    <input class="form-control mr-sm-2" type="search" placeholder="Pesquisar" aria-label="Pesquisar">
                    <button class="btn btn-outline-secondary my-2 my-sm-0" type="submit">Pesquisar</button>
                </form>
            </div>
        </nav>





        </br>
        </br>
    </nav>
    </br>
    </br>
<center>
    <div id="carouselExampleIndicators" class="carousel slide mb-4" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
            <div class="carousel-item active">
                <img class="d-block w-80 img-fluid" src="../../View/Imagens/img1.jpg" height= "1000px" weight= "600px" alt="First slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-80 img-fluid" src="../../View/Imagens/img2.jpg" height= "1000px" weight= "600px" alt="Second slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-80 img-fluid" src="../../View/Imagens/img4.jpg" height= "1000px" weight= "600px" alt="Third slide">
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Anterior</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Próximo</span>
        </a>
    </div>
</center>
<!-- Page Content -->
<div id="container-horizontal">
    <div id="container-horizontal">
        <div id="painel-botoes">
            <!-- Posição Material -->
            <div class="botao" >
                <div class="icone-botao b1">
                    <i class="glyphicon glyphicon-edit"></i>
                </div>
                <div class="nome-botao">
                    <a class="nav-link" href="../Cliente/Perfil_Cliente.php">Perfil</a>
                </div>
            </div>
            <!-- Pré-Solicitação -->
            <div class="botao" >
                <div class="icone-botao b2">
                    <i class="glyphicon glyphicon-shopping-cart"></i>
                </div>
                <div class="nome-botao">
                    <a href="./paginas/vendas/lista.html">Vendas</a>
                </div>
            </div>
            <!-- Saldo Política 4A -->
            <div class="botao" >
                <div class="icone-botao b3">
                    <i class="glyphicon glyphicon-usd"></i>
                </div>
                <div class="nome-botao">
                    Saldos
                </div>
            </div>
            <!-- Menu Planejamento -->
            <?php //if ($_SESSION['existe_estoque'] == 0) { ?>
                <div class="botao" >
                    <div class="icone-botao b4">
                        <i class="glyphicon glyphicon-list-alt"></i>
                    </div>
                    <div class="nome-botao">
                        <a class="nav-link" href="../../Controller/Gerente/Core.php">Criar Estoque</a>

                        <center>
                            <div id="carouselExampleIndicators" class="carousel slide mb-4" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                                </ol>
                                <div class="carousel-inner" role="listbox">
                                    <div class="carousel-item active">
                                        <img class="d-block w-80 img-fluid" src="../../View/Imagens/img1.jpg" height= "1000px" weight= "600px" alt="First slide">
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block w-80 img-fluid" src="../../View/Imagens/img2.jpg" height= "1000px" weight= "600px" alt="Second slide">
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block w-80 img-fluid" src="../../View/Imagens/img4.jpg" height= "1000px" weight= "600px" alt="Third slide">
                                    </div>

                                </div>
                            </div>
                        <?php// } ?>
                        <?php if ($_SESSION['existe_estoque'] == 1) { ?>
                            <div class="botao" >
                                <div class="icone-botao b4">
                                    <i class="glyphicon glyphicon-list-alt"></i>
                                </div>
                                <div class="nome-botao">
                                    <a class="nav-link" href="../../View/Gerente/Cadastro_Produto.php">Cadastrar Produto</a>
                                </div>
                            </div>
                        <?php } ?>
                        <!-- Atas Ativas -->
                        <div class="botao" >
                            <div class="icone-botao b5">
                                <i class="glyphicon glyphicon-time"></i>
                            </div>
                            <div class="nome-botao">
                                Histórico de Vendas
                            </div>
                        </div>
                        <!-- Processos na Assessoria de Licitação -->
                        <div class="botao" >
                            <div class="icone-botao b6">
                                <i class="glyphicon glyphicon-folder-close"></i>
                            </div>
                            <div class="nome-botao">
                                Processos na Assessoria de Licitação
                            </div>
                        </div>
                        <!-- Fornecedores do Material -->
                        <div class="botao" >
                            <div class="icone-botao b7">
                                <i class="glyphicon glyphicon-barcode"></i>
                            </div>
                            <div class="nome-botao">
                                Fornecedores do Material
                            </div>
                        </div>
                        <!-- Consulta OF -->
                        <div class="botao" >
                            <div class="icone-botao b8">
                                <i class="glyphicon glyphicon-search"></i>
                            </div>
                            <div class="nome-botao">
                                <a href="consult_pedid.php">consultar pedidos</a>
                            </div>
                        </div>
                        <!-- Menu UN -->
                        <div class="botao" >
                            <div class="icone-botao b9">
                                <i class="glyphicon glyphicon-th-list"></i>
                            </div>
                            <div class="nome-botao">
                                <a href="register_vend.php">Nova Solicitação</a>
                            </div>
                        </div>
                </div>

            </div>
        </div>
        <!-- /.row -->
    </div>
    <br>
    <br>
    <!-- Bootstrap core JavaScript -->
    <script src="../View/vendor/jquery/jquery.min.js"></script>
    <script src="../View/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- jQuery CDN - Slim version (=without AJAX) -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <!-- jQuery Custom Scroller CDN -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#sidebar").mCustomScrollbar({
                theme: "minimal"
            });

            $('#sidebarCollapse').on('click', function () {
                $('#sidebar, #content').toggleClass('active');
                $('.collapse.in').toggleClass('in');
                $('a[aria-expanded=true]').attr('aria-expanded', 'false');
            });
        });
    </script>

    <br>
    <br>
    </body>
    <!-- Bootstrap core JavaScript -->
    <script src="../View/vendor/jquery/jquery.min.js"></script>
    <script src="../View/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>



    <!-- jQuery CDN - Slim version (=without AJAX) -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <!-- jQuery Custom Scroller CDN -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#sidebar").mCustomScrollbar({
                theme: "minimal"
            });

            $('#sidebarCollapse').on('click', function () {
                $('#sidebar, #content').toggleClass('active');
                $('.collapse.in').toggleClass('in');
                $('a[aria-expanded=true]').attr('aria-expanded', 'false');
            });
        });
    </script>


</html>
<?php
//} else {
//}
?>

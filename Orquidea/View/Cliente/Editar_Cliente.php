<?php
require '../../Controller/verificar.php';
?>
<!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Orquídea || Informações do PerfilS</title>

        <!-- Bootstrap CSS CDN -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
        <!-- Our Custom CSS -->
        <link rel="stylesheet" href="../assets/css/style2.css">
        <!-- Scrollbar Custom CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">

        <!-- Font Awesome JS -->
        <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
        <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>

    </head>

    <body>

            <!-- Sidebar  -->
            <div class="wrapper">
                <!-- Sidebar  -->
                <nav id="sidebar">
                    <div class="sidebar-header">
                        <a href="../../View/Gerente/Home_Cliente.php"><img src="../Imagens/Logomarca.png" style="height: 115px; width: 115px; margin-left: 45px;"></a>
                    </div>
                    <ul class="list-unstyled CTAs components">
                        <li>
                            <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle" id="menu-item" >Menu</a>
                            <div class="collapse list-unstyled" id="pageSubmenu">
                                <span class="nav-item d-block">
                                    <a class="nav-link" id="menu-item" href="Home_Cliente.php">Início</a>
                                </span>
                                <span class="nav-item d-block active">
                                    <a class="nav-link" id="menu-item" href="Perfil_Cliente.php">Perfil</a>
                                </span>
                                <?php if ($_SESSION['existe_estoque'] == 0) { ?>                                
                                <span class="nav-item d-block">
                                    <a class="nav-link" id="menu-item" href="../../Controller/Gerente/Core.php">Estoque</a>
                                </span>
                                <?php } ?>
                                <?php if ($_SESSION['existe_estoque'] == 1) { ?>
                                    <?php $_SESSION['criar_produto'] = 1?>
                                <span class="nav-item d-block">
                                    <a class="nav-link" id="menu-item" href="../../Controller/Gerente/Core.php">Estoque</a>
                                </span> 
                                     <?php } ?>                           
                                <span class="nav-item d-block">
                                    <a class="nav-link" id="menu-item" href="paginas/Vendas/lista.html">Vendas</a>
                                </span>
                            </div>
                        </li>
                    </br>
                    </br>
                    </ul>
                    <span class="nav-item" style="margin-left: 10px;">
                            <a href="../Controller/logout.php" id="menu-item" >Sair</a>
                       </span>
                </nav>
            <!-- Page Content  -->
            <div id="content">


                <div class="container-fluid">

                    <button type="button" id="sidebarCollapse" class="btn btn-info">
                        <i class="fas fa-align-left"></i>
                        <span>>>>></span>
                    </button>
                </div>
                </br>
                </br>

                <form action="../../Controller/Cliente/ClienteController.php" method="POST">
                    <div class="form-group">
                        <label>Nome: </label>
                        <input type="text" class="form-control" name="nome_up" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="<?php echo $_SESSION['Perfil_nome'] ?>">
                        <small id="emailHelp" class="form-text text-muted">Digite seu nome completo</small>
                    </div>
                    <div class="form-group">
                        <label>CPF: </label>
                        <input type="text" class="form-control" name="cpf_up" id="exampleInputPassword1" placeholder="<?php echo $_SESSION['Perfil_cpf'] ?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email: </label>
                        <input type="email" class="form-control" name="email_up" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="<?php echo $_SESSION['Perfil_email'] ?>">
                    </div>
                    <div class="form-group">
                        <label>Telefone: </label>
                        <input type="text" class="form-control" name="telefone_up" id="exampleInputPassword1" placeholder="<?php echo $_SESSION['Perfil_telefone'] ?>">
                    </div>
                    <div class="form-group">
                        <label>Celular: </label>
                        <input type="text" class="form-control" name="celular_up" id="exampleInputPassword1" placeholder="<?php echo $_SESSION['Perfil_celular'] ?>">
                    </div>
                    <div class="form-group">
                        <label>Data de Nascimento: </label>
                        <input type="date" class="form-control" name="nascimento_up" id="exampleInputPassword1" placeholder="<?php echo $_SESSION['Perfil_nascimento'] ?>">
                    </div>
                    <input type="hidden" name="id" value="<?=$_SESSION['id']?>">
                    <input type="hidden" name="cad" value = "2">
                    <button type="submit" class="btn btn-primary"> Confirmar </button>
                </form>
                <!-- Footer -->
                <footer class="py-5 bg-secondary"  style="height: 5px;">
                    <div class="container" style="height: 5px;">
                        <p class="m-0 text-center text-white">Copyright &copy; Gestão de Supermercado 2019</p>
                    </div>
                </footer>
            </div>
            <!-- Bootstrap core JavaScript -->
            <script src="../vendor/jquery/jquery.min.js"></script>
            <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
            <!-- jQuery CDN - Slim version (=without AJAX) -->
            <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
            <!-- Popper.JS -->
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
            <!-- Bootstrap JS -->
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
            <!-- jQuery Custom Scroller CDN -->
            <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>

            <script type="text/javascript">
                $(document).ready(function () {
                    $("#sidebar").mCustomScrollbar({
                        theme: "minimal"
                    });

                    $('#sidebarCollapse').on('click', function () {
                        $('#sidebar, #content').toggleClass('active');
                        $('.collapse.in').toggleClass('in');
                        $('a[aria-expanded=true]').attr('aria-expanded', 'false');
                    });
                });
            </script>

    </body>
    <script>
        $(document).ready(function () {
            var $cpf_up = $("#cpf");
            $cpf_up.mask('000.000.000-00', {reverse: true});
        });
        $(document).ready(function () {
            var $telefone_up = $("#telefone_up");
            $telefone_up.mask('(00)0000-0000', {reverse: true});
        });
        $(document).ready(function () {
            var $celular_up = $("#celular_up");
            $celular_up.mask('(00)00000-0000', {reverse: true});
        });
        $(document).ready(function () {
            var $wcep = $("#cep");
            $wcep.mask('00.000-000', {reverse: true});
        });
    </script>
</html>

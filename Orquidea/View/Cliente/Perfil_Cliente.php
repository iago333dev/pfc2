<?php
require '../../Controller/Cliente/ClienteController.php';
include '../../Controller/verificar.php';
$controller2 = new ClienteController();

$info = $controller2->mostrar_dados($_SESSION['id']);
$_SESSION['Perfil_nome'] = $info[0]['nome'];
$_SESSION['Perfil_cpf'] = $info[0]['cpf'];
$_SESSION['Perfil_email'] = $info[0]['email'];
$_SESSION['Perfil_telefone'] = $info[0]['telefone'];
$_SESSION['Perfil_celular'] = $info[0]['celular'];
$_SESSION['Perfil_nascimento'] = $info[0]['nascimento'];
?>
<!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Orquídea || Informações do PerfilS</title>

        <!-- Bootstrap CSS CDN -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
        <!-- Our Custom CSS -->
        <link rel="stylesheet" href="../assets/css/style2.css">
        <!-- Scrollbar Custom CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">

        <!-- Font Awesome JS -->
        <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
        <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>

    </head>

    <body>

        <div class="wrapper">
            <!-- Sidebar  -->
                <nav id="sidebar">
                    <div class="sidebar-header">
                        <a href="../../View/Gerente/Home_Gerencia.php"><img src="../Imagens/Logomarca.png" style="height: 115px; width: 115px; margin-left: 45px;"></a>
                    </div>
                    <ul class="list-unstyled CTAs components">
                        <li>
                            <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle" id="menu-item" >Menu</a>
                            <div class="collapse list-unstyled" id="pageSubmenu">
                                <span class="nav-item d-block">
                                    <a class="nav-link" id="menu-item" href="Home_Gerencia.php">Início</a>
                                </span>
                                <span class="nav-item d-block active">
                                    <a class="nav-link" id="menu-item" href="Perfil_Gerencia.php">Perfil</a>
                                </span>
                                <?php if ($_SESSION['existe_estoque'] == 0) { ?>                                
                                <span class="nav-item d-block">
                                    <a class="nav-link" id="menu-item" href="../../Controller/Gerente/Core.php">Estoque</a>
                                </span>
                                <?php } ?>
                                <?php if ($_SESSION['existe_estoque'] == 1) { ?>
                                    <?php $_SESSION['criar_produto'] = 1?>
                                <span class="nav-item d-block">
                                    <a class="nav-link" id="menu-item" href="../../Controller/Gerente/Core.php">Estoque</a>
                                </span> 
                                     <?php } ?>                           
                                <span class="nav-item d-block">
                                    <a class="nav-link" id="menu-item" href="paginas/Vendas/lista.html">Vendas</a>
                                </span>
                            </div>
                        </li>
                    </br>
                    </br>
                    </ul>
                    <span class="nav-item" style="margin-left: 10px;">
                            <a href="../../Controller/logout.php" id="menu-item" >Sair</a>
                       </span>
                </nav>
            <!-- Page Content  -->
            <div id="content">


                <div class="container-fluid">

                    <button type="button" id="sidebarCollapse" class="btn btn-info">
                        <i class="fas fa-align-left"></i>
                        <span>>>>></span>
                    </button>
                </div>
                </br>
                </br>



                <div id="ProfilePage">

                    <table id="tableInfo">
                        <tr>
                            <th>Nome:</th>
                            <td><?php echo $_SESSION['Perfil_nome']; ?></td>
                        </tr>
                        <tr>
                            <th>CPF:</th>
                            <td><?php echo $_SESSION['Perfil_cpf']; ?></td>
                        </tr>
                        <tr>
                            <th>Email:</th>
                            <td><?php echo $_SESSION['Perfil_email']; ?></td>
                        </tr>
                        <tr>
                            <th>Telefone:</th>
                            <td><?php echo $_SESSION['Perfil_telefone']; ?></td>
                        </tr>
                        <tr>
                            <th>Celular:</th>
                            <td><?php echo $_SESSION['Perfil_celular']; ?></td>
                        </tr>
                        <tr>
                            <th>Data de nascimento:</th>
                            <td><?php echo $_SESSION['Perfil_nascimento']; ?></td>
                        </tr>
                    </table>

                    <!-- Needed because other elements inside ProfilePage have floats -->
                    <div style="clear:both"></div>
                </div>

                <br>
                <button type="button" class="btn btn-info">
                    <i class="fas fa-align-left"></i>
                    <a class="nav-link" href="Editar_Cliente.php">Editar informações</a>
                </button>
                <br>
                <!-- Footer -->
                <footer class="py-5 bg-secondary"  style="height: 5px;">
                    <div class="container" style="height: 5px;">
                        <p class="m-0 text-center text-white">Copyright &copy; Gestão de Supermercado 2019</p>
                    </div>
                </footer>
            </div>
            <!-- Bootstrap core JavaScript -->
            <script src="../vendor/jquery/jquery.min.js"></script>
            <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
            <!-- jQuery CDN - Slim version (=without AJAX) -->
            <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
            <!-- Popper.JS -->
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
            <!-- Bootstrap JS -->
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
            <!-- jQuery Custom Scroller CDN -->
            <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>

            <script type="text/javascript">
                $(document).ready(function () {
                    $("#sidebar").mCustomScrollbar({
                        theme: "minimal"
                    });

                    $('#sidebarCollapse').on('click', function () {
                        $('#sidebar, #content').toggleClass('active');
                        $('.collapse.in').toggleClass('in');
                        $('a[aria-expanded=true]').attr('aria-expanded', 'false');
                    });
                });
            </script>

    </body>


</html>
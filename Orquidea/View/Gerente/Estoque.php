<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php
require '../../Controller/Gerente/Estoque.php';

//require './Home_Gerencia.php';

//session_start();
/*
$_SESSION['nivel'] = "Adm";
$_SESSION['user'] = "OrquideaEmpresa";
$_SESSION['existe_estoque'] = 1;

//Controller Descontinuada...
//require '../../Controller/Gerente/Core.php';




require '../../Controller/verificar.php';

/*

if ($_SESSION['nivel'] == 'Adm' || $_SESSION['nivel'] == 'Funcionario') {
    $contador = 0;
    $repetidor = 0;
    if(isset($_SESSION['limite'])){
        $limite = $_SESSION['limite'];
    }else{
        $limite = 0;
           
    }
    
   // var_dump($limite);

  */  
    ?>
    <!DOCTYPE html>
    <html>
        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <title>Orquídea || Estoque</title>
            <!-- Bootstrap CSS CDN -->
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
            <!-- Our Custom CSS -->
          <link rel="stylesheet" href="../assets/css/style2.css"> 
            <!-- Scrollbar Custom CSS -->
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
            <!-- Font Awesome JS -->
            <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
            <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>
        </head>
        <body>
            <div class="wrapper">
                <!-- Sidebar  -->
                <nav id="sidebar">
                    <div class="sidebar-header">
                        <a href="../../View/Gerente/Home_Gerencia.php"><img src="../Imagens/Logomarca.png" style="height: 115px; width: 115px; margin-left: 45px;"></a>
                    </div>
                    <ul class="list-unstyled CTAs components">
                        <li>
                            <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="true" class="dropdown-toggle" id="menu-item" >Menu</a>
                            <div class="collapse list-unstyled" id="pageSubmenu">
                                <span class="nav-item d-block">
                                    <a class="nav-link" id="menu-item" href="Home_Gerencia.php">Início</a>
                                </span>
                                <span class="nav-item d-block active">
                                    <a class="nav-link" id="menu-item" href="Perfil_Gerencia.php">Perfil</a>
                                </span>
                                <span class="nav-item d-block">
                                    <a class="nav-link" id="menu-item" href="Estoque.php">Estoque</a>
                                </span>

                            </div>
                        </li>
                        </br>
                        </br>
                    </ul>
                    <span class="nav-item" style="margin-left: 10px;">
                        <a href="../../Controller/logout.php" id="menu-item" >Sair</a>
                    </span>
                </nav>




                    </br>
                    </br>                          

                    <table class="table">
                        <thead>
                            <tr>
                                <th>Produtos:</th>
                                <th>Data de Entrada:</th>
                                <th>Data de Validade:</th>
                                <th>Preço de Fábrica:</th>
                                <th>Preço de Varejo:</th>
                                <th>Marca:</th>
                                <th>Categoria:</th>
                                <th>Quantidade:</th>
                                <th>Imagem Principal:</th>
                            </tr>
                        </thead>
                        <tbody>
      <?php if (!empty($Produtos)):
        foreach ($Produtos as $row): ?>
        <td>
          <?php echo $row["nome"]?>
        </td>
        <td>
          <?php echo $row["data_entrada"]?>
        </td>
        <td>
          <?php echo $row["validade"]?>
        </td>
        <td>
          <?php echo $row["preco_compra"]?>
        </td>
        <td>
          <?php echo $row["preco_venda"]?>
        </td>
        <td>
          <?php echo $row["Marca"]?>
        </td>
        <td>
          <?php echo $row["Categoria"]?>
        </td>
        <td>
          <?php echo $row["Quantidade"]?>
        </td>

      <?php endforeach; ?>
    <?php else: {
      echo "<td colspan='5' align = 'center'>
      Você ainda não possui nenhum locatário cadastrado...
      </td>";
    } ?>
  <?php endif; ?>
  </tbody>
                        



                        
                    </table>
                    <!-- Footer -->
                    <footer class="py-5 bg-secondary"  style="height: 5px;">
                        <div class="container" style="height: 5px;">
                            <p class="m-0 text-center text-white"> Copyright &copy; Gestão de Supermercado 2019</p>
                            
                    <div class="container-fluid">
                        </div>
                    </footer>
                </div>

                <!-- Bootstrap core JavaScript -->
                <script src="../View/vendor/jquery/jquery.min.js"></script>
                <script src="../View/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
                <!-- jQuery CDN - Slim version (=without AJAX) -->
                <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
                <!-- Popper.JS -->
                <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
                <!-- Bootstrap JS -->
                <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
                <!-- jQuery Custom Scroller CDN -->
                <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
                <script type="text/javascript">
                    $(document).ready(function () {
                        $("#sidebar").mCustomScrollbar({
                            theme: "minimal"
                        });

                        $('#sidebarCollapse').on('click', function () {
                            $('#sidebar, #content').toggleClass('active');
                            $('.collapse.in').toggleClass('in');
                            $('a[aria-expanded=true]').attr('aria-expanded', 'false');
                        });
                    });
                </script>
                <div id="content">
                        <button type="button" id="sidebarCollapse" class="btn btn-info">
                            <i class="fas fa-align-left"></i>
                            <span>Menu</span>
                        </button>
                    </div>
        </body>
    </html>


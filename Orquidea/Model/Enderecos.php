<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Enderecos
 *
 * @author Alisson Soares
 */
class Enderecos {
    //put your code here
    
    private $id;
    private $logradouro;
    private $bairro;
    private $cep;
    private $cidade;
    private $uf;
    private $pais;
    private $id_login;
    
    public function getId() {
        return $this->id;
    }

    public function getLogradouro() {
        return $this->logradouro;
    }

    public function getBairro() {
        return $this->bairro;
    }

    public function getCep() {
        return $this->cep;
    }

    public function getCidade() {
        return $this->cidade;
    }

    public function getUf() {
        return $this->uf;
    }

    public function getPais() {
        return $this->pais;
    }

    public function getId_login() {
        return $this->id_login;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setLogradouro($logradouro) {
        $this->logradouro = $logradouro;
    }

    public function setBairro($bairro) {
        $this->bairro = $bairro;
    }

    public function setCep($cep) {
        $this->cep = $cep;
    }

    public function setCidade($cidade) {
        $this->cidade = $cidade;
    }

    public function setUf($uf) {
        $this->uf = $uf;
    }

    public function setPais($pais) {
        $this->pais = $pais;
    }

    public function setId_login($id_login) {
        $this->id_login = $id_login;
    }
}

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ClientesDao
 *
 * @author Alisson Soares
 */
class ClientesDao {

    //put your code here

    public function insert_dados_cliente($nome, $cpf, $email, $telefone, $celular, $nascimento, $id_login) {
        $conn = new DatabaseUtility();
        $conn->connect();
        $conn->pdo->query("INSERT INTO cliente (id,nome,cpf,email,telefone,celular,nascimento,login_id) VALUES (DEFAULT,'$nome','$cpf','$email','$telefone','$celular','$nascimento','$id_login')");
    }

    public function select_nome_cliente($id_login) {
        $conn = new DatabaseUtility();
        $conn->connect();
        $query = $conn->pdo->query("SELECT * FROM cliente WHERE login_id = $id_login");
        while ($linha = $query->fetch(PDO::FETCH_ASSOC)) {
            return $linha['nome'];
        }
        $conn->disconect();
    }
    
    public function show($id){
        $conn = new DatabaseUtility();
        $conn->connect();
        $query = $conn->pdo->query("SELECT * FROM cliente WHERE login_id = '$id'");
        while ($linha = $query->fetch(PDO::FETCH_ASSOC)) {
            $dados[] = $linha;
        }
        return $dados;
    }
    
    public function update_nome($id, $nome) {
        $conn = new DatabaseUtility();
        $conn->connect();
        $conn->pdo->query("UPDATE cliente SET nome = '$nome' WHERE id = $id");
    }
    
    public function update_cpf($id, $cpf) {
        $conn = new DatabaseUtility();
        $conn->connect();
        $conn->pdo->query("UPDATE cliente SET cpf = '$cpf' WHERE id = '$id'");
    }
    
    public function update_email($id, $email) {
        $conn = new DatabaseUtility();
        $conn->connect();
        $conn->pdo->query("UPDATE cliente SET email = '$email' WHERE id = '$id'");
    }
    
    public function update_telefone($id, $telefone) {
        $conn = new DatabaseUtility();
        $conn->connect();
        $conn->pdo->query("UPDATE cliente SET telefone = '$telefone' WHERE id = '$id'");
    }
    
    public function update_celular($id, $celular) {
        $conn = new DatabaseUtility();
        $conn->connect();
        $conn->pdo->query("UPDATE cliente SET celular = '$celular' WHERE id = '$id'");
    }
    
    public function update_nascimento($id, $nascimento) {
        $conn = new DatabaseUtility();
        $conn->connect();
        $conn->pdo->query("UPDATE cliente SET nascimento = '$nascimento' WHERE id = '$id'");
    }
}

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Clientes
 *
 * @author Alisson Soares
 */
class Clientes {
    //put your code here
    private $id;
    private $nome;
    private $cpf;
    private $email;
    private $telefone;
    private $celular;
    private $nascimento;
    private $id_login;
    
    public function getId() {
        return $this->id;
    }

    public function getNome() {
        return $this->nome;
    }

    public function getCpf() {
        return $this->cpf;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getTelefone() {
        return $this->telefone;
    }

    public function getCelular() {
        return $this->celular;
    }

    public function getNascimento() {
        return $this->nascimento;
    }

    public function getId_login() {
        return $this->id_login;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setNome($nome) {
        $this->nome = $nome;
    }

    public function setCpf($cpf) {
        $this->cpf = $cpf;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function setTelefone($telefone) {
        $this->telefone = $telefone;
    }

    public function setCelular($celular) {
        $this->celular = $celular;
    }

    public function setNascimento($nascimento) {
        $this->nascimento = $nascimento;
    }

    public function setId_login($id_login) {
        $this->id_login = $id_login;
    }
}

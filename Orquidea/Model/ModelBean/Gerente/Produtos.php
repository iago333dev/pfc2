<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Produtos
 *
 * @author Alisson Soares
 */
class Produtos {
    //put your code here
    private $id;
    private $nome;
    private $validade;
    private $dataen;
    private $precocompra;
    private $precovenda;
    private $marca;
    private $categoria;
    private $quant;
    private $imagem1;
    private $imagem2;
    private $imagem3;
    private $idestoque;
    private $idempresa;
    
    public function getId() {
        return $this->id;
    }

    public function getNome() {
        return $this->nome;
    }

    public function getValidade() {
        return $this->validade;
    }

    public function getDataen() {
        return $this->dataen;
    }

    public function getPrecocompra() {
        return $this->precocompra;
    }

    public function getPrecovenda() {
        return $this->precovenda;
    }

    public function getMarca() {
        return $this->marca;
    }

    public function getCategoria() {
        return $this->categoria;
    }

    public function getQuant() {
        return $this->quant;
    }

    public function getImagem1() {
        return $this->imagem1;
    }

    public function getImagem2() {
        return $this->imagem2;
    }

    public function getImagem3() {
        return $this->imagem3;
    }

    public function getIdestoque() {
        return $this->idestoque;
    }

    public function getIdempresa() {
        return $this->idempresa;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setNome($nome) {
        $this->nome = $nome;
    }

    public function setValidade($validade) {
        $this->validade = $validade;
    }

    public function setDataen($dataen) {
        $this->dataen = $dataen;
    }

    public function setPrecocompra($precocompra) {
        $this->precocompra = $precocompra;
    }

    public function setPrecovenda($precovenda) {
        $this->precovenda = $precovenda;
    }

    public function setMarca($marca) {
        $this->marca = $marca;
    }

    public function setCategoria($categoria) {
        $this->categoria = $categoria;
    }

    public function setQuant($quant) {
        $this->quant = $quant;
    }

    public function setImagem1($imagem1) {
        $this->imagem1 = $imagem1;
    }

    public function setImagem2($imagem2) {
        $this->imagem2 = $imagem2;
    }

    public function setImagem3($imagem3) {
        $this->imagem3 = $imagem3;
    }

    public function setIdestoque($idestoque) {
        $this->idestoque = $idestoque;
    }

    public function setIdempresa($idempresa) {
        $this->idempresa = $idempresa;
    }
}

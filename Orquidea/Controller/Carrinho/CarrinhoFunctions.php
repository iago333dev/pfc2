<?php


	//include '../../Model/ModelDao/database.php';
	include '../../Model/ModelDao/Carrinho/CarrinhoDao.php';

// Metodo para Fechar Venda

/*

else

*/

if (isset($_POST["buyon"])) {
	//Calcular Valor total
	session_start();
	$resultsCarts = getContentCart();
	$total=0;
	foreach($resultsCarts as $result){
		//echo 'Nome do Produto: '.$result['name'].'</br>';
		//echo 'Preço: '.$result['price'].'</br>';
		//echo 'Quantidade: '.$result['quantity'].'</br'
			$total = $total+$result['price']*$result['quantity'];
	}


			$codigo = md5($_POST['unam']);
			$data_entrada = $today = date("Y-m-d");
			$data_entrega = $_POST['date'];
			$login_id = $_POST['logid'];
			$mer_id = $_GET["mer_id"];

	$novacompra = new CarrinhoDao();
	$novacompra->FinalizarCompra($data_entrada,$data_entrega,$total,$login_id,$mer_id);

}











if(!isset($_SESSION['carrinho'])) {
	$_SESSION['carrinho'] = array();
}

function addCart($id, $quantity) {
	if(!isset($_SESSION['carrinho'][$id])){
		$_SESSION['carrinho'][$id] = $quantity;
	}
}

function deleteCart($id) {
	if(isset($_SESSION['carrinho'][$id])){
		unset($_SESSION['carrinho'][$id]);
	}
}

function updateCart($id, $quantity) {
	if(isset($_SESSION['carrinho'][$id])){
		if($quantity > 0) {
			$_SESSION['carrinho'][$id] = $quantity;
		} else {
		 	deleteCart($id);
		}
	}
}

function getContentCart() {


	$results = array();

	if($_SESSION['carrinho']) {

		$cart = $_SESSION['carrinho'];

		$ok = new CarrinhoDao();

		$products =  $ok->GetProdutoByID(implode(',', array_keys($cart)));

		foreach($products as $product) {

			$results[] = array(
							  'id' => $product['id'],
							  'name' => $product['nome'],
							  'price' => $product['preco_venda'],
							  'quantity' => $cart[$product['id']],
							  'subtotal' => $cart[$product['id']] * $product['preco_venda'],
						);
		}
	}

	return $results;
}

function getTotalCart() {

	$conn = new DatabaseUtility();
	$pdo = $conn->connect();

	$total = 0;

	foreach(getContentCart() as $product) {
		$total += $product['subtotal'];
	}
	return $total;
}

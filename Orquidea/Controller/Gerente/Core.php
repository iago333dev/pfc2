<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once '../Gerente/Controller.php';

session_start();

$controller = new Controller();

if (filter_input(INPUT_POST, "cad") == md5("Cadastro_Empresarial")) {
    $cadastrar = $controller->cadastrar();
    if ($cadastrar == 1) {
        //Aviso de Cadastro bem sucedido 
        echo "<script>alert('Cadastro Bem Sucedido!');";
        //Redirecionando para página de Login
        echo "window.location.href= '../../View/index.php';</script>";
    }
}

if ($_SESSION['existe_estoque'] == 0) {
    $controller->criar_estoque($_SESSION['user']);
    $_SESSION['existe_estoque'] = 1;
    echo "<script>alert('Estoque Criado');";
    echo "window.location.href= '../../View/Gerente/Home_Gerencia.php';</script>";
}

if(filter_input(INPUT_POST, "cad") == md5("editar_dados_gerencia")){
    $editou = $controller->editar($_SESSION['id'], $_POST['nome_up'], $_POST['cpf_up'], $_POST['email_up'], $_POST['telefone_up'], $_POST['celular_up'], $_POST['nascimento_up']);
    if($editou == 1){
        echo "<script>alert('Edição de Perfil Concluída');";
            echo "window.location.href= '../../View/Gerente/Perfil_Gerencia.php';</script>";
    }
}
/*
if($_SESSION['existe_estoque'] == 1 && $_SESSION['criar_produto'] == 1){
    $_SESSION['Produtos'] = $controller->exibir_produtos();
    echo "<script>window.location.href= '../../View/Gerente/Estoque.php';</script>";
    $_SESSION['criar_produto'] = 1;
}
*/
if($_SESSION['criar_produto'] == 2){
    $_SESSION['limite'] = filter_input(INPUT_POST, 'limite', FILTER_SANITIZE_STRING);
    echo "<script>window.location.href= '../../View/Gerente/Estoque.php';</script>";
}

if(filter_input(INPUT_POST, "cad") == md5("Cadastro_Produto")){
    $controller->cadastrar_produto();
}
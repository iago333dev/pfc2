<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//Incluindo arquivo de verificação
//include '../../Controller/verificar.php';
//Incluindo Classes do ModelBean
include '../../Model/ModelBean/Gerente/Funcionarios.php';
include '../../Model/ModelBean/Login.php';
include '../../Model/Enderecos.php';
include '../../Model/ModelBean/Gerente/Empresa.php';
include '../../Model/ModelBean/Gerente/Produtos.php';

//Incluindo Classes do ModelDao
include '../../Model/ModelDao/LoginDao.php';
include '../../Model/ModelDao/Gerente/FuncionariosDao.php';
include '../../Model/ModelDao/EnderecoDao.php';
include '../../Model/ModelDao/Gerente/EmpresaDao.php';
include '../../Model/ModelDao/Gerente/ProdutosDao.php';

class Controller {

    public function cadastrar() {
        //Instanciando Classes do ModelBean
        $funcionarios_bean = new Funcionarios();
        $login_bean = new Login();
        $endereco_bean = new Enderecos();
        $empresa_bean = new Empresa();

        //Instanciando Classes do ModelDao
        $login_dao = new LoginDao();
        $funcionarios_dao = new FuncionariosDao();
        $endereco_dao = new EnderecoDao();
        $empresa_dao = new EmpresaDao();
        $teste = filter_input(INPUT_POST, "cad");

        if (!empty($_POST["wnome"]) && !empty($_POST["wcpf"]) && !empty($_POST["wemail"]) && !empty($_POST["wtel1"]) && !empty($_POST["wtel2"]) && !empty($_POST["wender"]) && !empty($_POST["wbair"]) && !empty($_POST["wuf"]) && !empty($_POST["wpais"]) && !empty($_POST["wnasc"]) && !empty($_POST["wlog"]) && !empty($_POST["wsen"])) {
            //Encapsulando os Dados de Login do Cliente
            $login_bean->setLogin(filter_input(INPUT_POST, 'wlog', FILTER_DEFAULT));
            $login_bean->setSenha(filter_input(INPUT_POST, 'wsen', FILTER_DEFAULT));
            //Encapsulando os Dados do Gerente
            //Dados Pessoais
            $funcionarios_bean->setNome(filter_input(INPUT_POST, 'wnome', FILTER_SANITIZE_STRING));
            $funcionarios_bean->setCpf(filter_input(INPUT_POST, 'wcpf', FILTER_SANITIZE_STRING));
            $funcionarios_bean->setEmail(filter_input(INPUT_POST, 'wemail', FILTER_SANITIZE_STRING));
            $funcionarios_bean->setTelefone(filter_input(INPUT_POST, 'wtel1', FILTER_SANITIZE_STRING));
            $funcionarios_bean->setCelular(filter_input(INPUT_POST, 'wtel2', FILTER_SANITIZE_STRING));
            $funcionarios_bean->setNascimento(filter_input(INPUT_POST, 'wnasc', FILTER_SANITIZE_STRING));
            //Endereço
            $endereco_bean->setLogradouro(filter_input(INPUT_POST, 'wender', FILTER_SANITIZE_STRING));
            $endereco_bean->setBairro(filter_input(INPUT_POST, 'wbair', FILTER_SANITIZE_STRING));
            $endereco_bean->setCep(filter_input(INPUT_POST, 'wcep', FILTER_SANITIZE_STRING));
            $endereco_bean->setCidade(filter_input(INPUT_POST, 'wcid', FILTER_SANITIZE_STRING));
            $endereco_bean->setUf(filter_input(INPUT_POST, 'wuf', FILTER_SANITIZE_STRING));
            $endereco_bean->setPais(filter_input(INPUT_POST, 'wpais', FILTER_SANITIZE_STRING));
            //Empresa
            $empresa_bean->setNome(filter_input(INPUT_POST, 'wemnome', FILTER_SANITIZE_STRING));
            $empresa_bean->setCnpj(filter_input(INPUT_POST, 'wemcnpj', FILTER_SANITIZE_STRING));
            if ($login_dao->comparar_login($login_bean->getLogin()) == 0) {
                $login_dao->insert_funcionario_login($login_bean->getLogin(), $login_bean->getSenha());
                //Regatando o ID de Login
                $login_bean->setId($login_dao->id_login($login_bean->getLogin(), $login_bean->getSenha()));
                //Inserindo dados na tabela de Empresa
                $empresa_dao->insert_dados_empresa($empresa_bean->getNome(), $empresa_bean->getCnpj());
                //Resgatando o ID da Empresa
                $empresa_bean->setId($empresa_dao->id_empresa($empresa_bean->getNome(), $empresa_bean->getCnpj()));
                //Inserindo dados na tabela de Funcionario
                $funcionarios_dao->insert_dados_funcionario($funcionarios_bean->getNome(), $funcionarios_bean->getCpf(), $funcionarios_bean->getEmail(), $funcionarios_bean->getTelefone(), $funcionarios_bean->getCelular(), $funcionarios_bean->getNascimento(), $login_bean->getId(), $empresa_bean->getId());
                //Inserindo dados na tabela de endereco
                $endereco_dao->insert_endereco($endereco_bean->getLogradouro(), $endereco_bean->getBairro(), $endereco_bean->getCep(), $endereco_bean->getCidade(), $endereco_bean->getUf(), $endereco_bean->getPais(), $login_bean->getId());
                return 1;
            } else {
                echo "login já existente";
                echo "<br>";
                echo "<a href = ../../View/index.php> Retorne </a>";
            }
        }
    }

    public function editar($id, $nome, $cpf, $email, $telefone, $celular, $nascimento) {
        $funcionarios_dao = new FuncionariosDao();
        if (!empty($id) && !empty($nome)) {
            $funcionarios_dao->update_nome($id, $nome);
        }
        if (!empty($id) && !empty($cpf)) {
            $funcionarios_dao->update_cpf($id, $cpf);
        }
        if (!empty($id) && !empty($email)) {
            $funcionarios_dao->update_email($id, $email);
        }
        if (!empty($id) && !empty($telefone)) {
            $funcionarios_dao->update_telefone($id, $telefone);
        }
        if (!empty($id) && !empty($celular)) {
            $funcionarios_dao->update_celular($id, $celular);
        }
        if (!empty($id) && !empty($nascimento)) {
            $funcionarios_dao->update_nascimento($id, $nascimento);
        }
        return 1;
    }

    public function criar_estoque($user) {
        $funcionarios_dao = new FuncionariosDao();
        $empresao_dao = new EmpresaDao();
        $empresao_dao->estoque($funcionarios_dao->find_id_empresa($user));
        return;
    }

    public function cadastrar_produto() {
        if (!empty($_POST["nome_prod"]) && !empty($_POST['validade']) && !empty($_POST['data_entrada']) && !empty($_POST['pre_fab_prod']) && !empty($_POST['pre_ven_prod']) && !empty($_POST['marc_prod']) && !empty($_POST['cat_prod']) && !empty($_POST['quant_prod']) && !empty($_FILES['img1_prod'])) {
            if ($_POST['quant_prod'] >= 1) {
                //Instanciando Classe Produto do ModelBean
                $Produto = new Produtos();

                //Instanciando Classe Funcionarios do ModelDao
                $funcionarios_dao = new FuncionariosDao();

                //Instanciando Classe Empresa do ModelDao
                $Empresa_dao = new EmpresaDao();

                //Instanciando Classe Produto do ModelDao
                $produto_dao = new ProdutosDao();

                //Encapsulando Dados do Produto
                $Produto->setNome(filter_input(INPUT_POST, 'nome_prod', FILTER_SANITIZE_STRING));
                $Produto->setValidade(filter_input(INPUT_POST, 'validade', FILTER_SANITIZE_STRING));
                $Produto->setDataen(filter_input(INPUT_POST, 'data_entrada', FILTER_SANITIZE_STRING));
                $Produto->setPrecocompra(filter_input(INPUT_POST, 'pre_fab_prod', FILTER_DEFAULT));
                $Produto->setPrecovenda(filter_input(INPUT_POST, 'pre_ven_prod', FILTER_DEFAULT));
                $Produto->setMarca(filter_input(INPUT_POST, 'marc_prod', FILTER_SANITIZE_STRING));
                $Produto->setCategoria(filter_input(INPUT_POST, 'cat_prod', FILTER_SANITIZE_STRING));
                $Produto->setQuant(filter_input(INPUT_POST, 'quant_prod', FILTER_SANITIZE_NUMBER_INT));
                //Tratando imagens
                if ($_FILES['img1_prod']) {
                    if ($_FILES['img1_prod']['size'] != 0) {
                        $extensao = strtolower(substr($_FILES['img1_prod']['name'], -4)); //Pega a Extensão da imagem
                        $Imagem1 = md5(time()) . $extensao; //Define o nome do arquivo
                        //mkdir("");
                        $diretorio = "../../View/Imagens/Produtos/"; //Define a pasta pra onde será enviado o arquivo 
                        move_uploaded_file($_FILES['img1_prod']['tmp_name'], $diretorio . $Imagem1); //efetua o upload
                    } else {
                        echo "Tamanho maior que o suportado";
                    }
                }
                //Pegando o ID da Empresa
                $Produto->setIdempresa($funcionarios_dao->find_id_empresa($_SESSION['user']));
                echo $Produto->getIdempresa();
                echo "<br>";
                //Pegando ID do Estoque
                $Produto->setIdestoque($Empresa_dao->find_id_estoque($Produto->getIdempresa()));
                echo $Produto->getIdestoque();
                if ($produto_dao->cadastrar_produto($Produto->getNome(), $Produto->getValidade(), $Produto->getDataen(), $Produto->getPrecocompra(), $Produto->getPrecovenda(), $Produto->getMarca(), $Produto->getCategoria(), $Produto->getQuant(), $Imagem1, $Produto->getIdestoque(), $Produto->getIdempresa()) == 1) {
                    echo "<script>window.location.href= '../../View/Gerente/Home_Gerencia.php';";
                    echo "alert('Produto Cadastrado');</script>";
                }
            } else {
                echo "<script>alert('Para que o produto seja cadastrado é necessário que exista quantidade suficiente');";
                echo "window.location.href= '../../View/Gerente/Home_Gerencia.php';</script>";
            }
        } else {
            echo "<script>alert('Existem Campos Vazios');";
            echo "window.location.href= '../../View/Gerente/Cadastro_Produto.php';</script>";
        }
    }
/*
    public function exibir_produtos() {
        //Instanciando Classe Produtos da ModelDao
        $produto_dao = new ProdutosDao();

        //Selecionando todos os produtos
        $produtos = $produto_dao->select_produtos();

        return $produtos;
    }
*/
    public function existe_estoque() {
        //Instanciando Classes da ModelDao
        $funcionario_dao = new FuncionariosDao();
        $empresa_dao = new EmpresaDao();

        //Busca o estoque da empresa logada
        if ($empresa_dao->find_id_estoque($funcionarios_dao->find_id_empresa($_SESSION['user']))) {
            return 1;
        }
    }

    public function mostrar_dados($id) {
        $funcionarios_dao = new FuncionariosDao();
        return $funcionarios_dao->show($id);
    }

    public function mostrar_empresa($id_empresa) {
        $empresa_dao = new EmpresaDao();
        return $empresa_dao->find_name_empresa($id_empresa);
    }

}

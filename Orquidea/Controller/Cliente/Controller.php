<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//Incluindo Classes do ModelBean
include '../../Model/ModelBean/Cliente/Clientes.php';
include '../../Model/ModelBean/Login.php';
include '../../Model/Enderecos.php';

//Incluindo Classes do ModelDao
include '../../Model/ModelDao/LoginDao.php';
include '../../Model/ModelDao/Cliente/ClientesDao.php';
include '../../Model/ModelDao/EnderecoDao.php';

$teste = filter_input(INPUT_POST, "cad");

if ($teste == 1) {
    $controller = new controller();
    $controller->cadastrar();
}

class controller {

    public function cadastrar() {
        //Instanciando Classes do ModelBean
        $clientes_bean = new Clientes();
        $login_bean = new Login();
        $endereco_bean = new Enderecos();
        //Instanciando Classes do ModelDao
        $login_dao = new LoginDao();
        $clientes_dao = new ClientesDao();
        $endereco_dao = new EnderecoDao();
        //Verificando se todos os campos foram preenchidos
        if (!empty($_POST["wnome"]) && !empty($_POST["wcpf"]) && !empty($_POST["wemail"]) && !empty($_POST["wtel1"]) && !empty($_POST["wtel2"]) && !empty($_POST["wender"]) && !empty($_POST["wbair"]) && !empty($_POST["wuf"]) && !empty($_POST["wpais"]) && !empty($_POST["wnasc"]) && !empty($_POST["wlog"]) && !empty($_POST["wsen"])) {
            //Encapsulando os Dados de Login do Cliente
            $login_bean->setLogin(filter_input(INPUT_POST, 'wlog', FILTER_DEFAULT));
            $login_bean->setSenha(filter_input(INPUT_POST, 'wsen', FILTER_DEFAULT));
            //Encapsulando os Dados do Cliente
            //Dados Pessoais
            $clientes_bean->setNome(filter_input(INPUT_POST, 'wnome', FILTER_SANITIZE_STRING));
            $clientes_bean->setCpf(filter_input(INPUT_POST, 'wcpf', FILTER_SANITIZE_NUMBER_INT));
            $clientes_bean->setEmail(filter_input(INPUT_POST, 'wemail', FILTER_SANITIZE_STRING));
            $clientes_bean->setTelefone(filter_input(INPUT_POST, 'wtel1', FILTER_SANITIZE_STRING));
            $clientes_bean->setCelular(filter_input(INPUT_POST, 'wtel2', FILTER_SANITIZE_STRING));
            $clientes_bean->setNascimento(filter_input(INPUT_POST, 'wnasc', FILTER_SANITIZE_STRING));
            //Endereço
            $endereco_bean->setLogradouro(filter_input(INPUT_POST, 'wender', FILTER_SANITIZE_STRING));
            $endereco_bean->setBairro(filter_input(INPUT_POST, 'wbair', FILTER_SANITIZE_STRING));
            $endereco_bean->setCep(filter_input(INPUT_POST, 'wcep', FILTER_SANITIZE_STRING));
            $endereco_bean->setCidade(filter_input(INPUT_POST, 'wcid', FILTER_SANITIZE_STRING));
            $endereco_bean->setUf(filter_input(INPUT_POST, 'wuf', FILTER_SANITIZE_STRING));
            $endereco_bean->setPais(filter_input(INPUT_POST, 'wpais', FILTER_SANITIZE_STRING));
            //Inserindo dados no banco
            //Inserindo dados na tabela de Login
            if ($login_dao->comparar_login($login_bean->getLogin()) == 0) {
                $login_dao->insert_cliente_login($login_bean->getLogin(), $login_bean->getSenha());
                //Resgatando o ID de login
                $login_bean->setId($login_dao->id_login($login_bean->getLogin(), $login_bean->getSenha()));
                //echo "<a href = '../../View/index.php'> Retorne </a>";
                //Inserindo dados na tabela de Cliente
                $clientes_dao->insert_dados_cliente($clientes_bean->getNome(), $clientes_bean->getCpf(), $clientes_bean->getEmail(), $clientes_bean->getTelefone(), $clientes_bean->getCelular(), $clientes_bean->getNascimento(), $login_bean->getId());
                //Inserindo dados na tabela de endereco
                $endereco_dao->insert_endereco($endereco_bean->getLogradouro(), $endereco_bean->getBairro(), $endereco_bean->getCep(), $endereco_bean->getCidade(), $endereco_bean->getUf(), $endereco_bean->getPais(), $login_bean->getId());
                //Redirecionando para página de Login
                header('Location: ../../View/index.php');
            } else {
                echo "login já existente";
                echo "<br>";
                echo "<a href = ../../View/index.php> Retorne </a>";
            }
        }
    }

    public function editar() {
        
    }
}
